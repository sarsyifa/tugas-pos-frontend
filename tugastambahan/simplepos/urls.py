from django.urls import path
from django.conf.urls import include, url
from . import views
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

appname= 'simplepos'
urlpatterns = [
    path('', views.index, name='index'),
    ]
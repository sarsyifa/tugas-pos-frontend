1. Install PIP
Open a command prompt and execute easy_install pip. 
This will install pip on your system. This command will work if you have 
successfully installed Setuptools.

Alternatively, go to http://www.pip-installer.org/en/latest/installing.html 
for installing/upgrading instructions.

2. Install Django
execute the following command: `pip install django`

3. Clone the repository to your local machine: `git clone https://gitlab.com/sarsyifa/tugas-pos-frontend.git`

4. Back to your local  work repository, then: `pip install -r requirements.txt`
5. Execute the webapp using `python manage.py runserver`
